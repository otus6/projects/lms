provider "google" {
  credentials = file("otus-hw.json")
  project = var.gcp_project
  region  = var.gcp_region
}
