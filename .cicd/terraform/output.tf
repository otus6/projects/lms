output "vm-name" {
  value = google_compute_instance.lms-terraform-ansible.name
}

output "vm-external-ip" {
  value = google_compute_instance.lms-terraform-ansible.network_interface.0.access_config.0.nat_ip
}

output "vm-internal-ip" {
  value = google_compute_instance.lms-terraform-ansible.network_interface.0.network_ip
}

# DNS
output "google_dns_managed_zone_name" {
    description = "Name of google dns managed zone"
    value       = "${google_dns_managed_zone.inpublic.*.name}"
}

output "google_dns_managed_zone_name_servers" {
    description = "The list of nameservers that will be authoritative for this domain. Use NS records to redirect from your DNS provider to these names, thus making Google Cloud DNS authoritative for this zone."
    value       = "${google_dns_managed_zone.inpublic.name_servers}"
}

output "google_dns_managed_zone_id" {
    description = "ID"
    value       = "${google_dns_managed_zone.inpublic.id}"
}

output "google_dns_record_set_name" {
    description = "Name"
    value       = "${google_dns_record_set.lms-terraform-ansible.name}"
}

output "google_dns_record_set_id" {
    description = "ID"
    value       = "${google_dns_record_set.lms-terraform-ansible.id}"
}

output "name_servers" {
    value = google_dns_managed_zone.inpublic.name_servers
}