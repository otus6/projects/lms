# allow http and https traffic
resource "google_compute_firewall" "allow-http" {
  name = "${var.app_name}-fw-allow-web"
  network = google_compute_network.vpc.name
  dynamic "allow" {
    for_each = ["80", "443"]
    content {
      protocol = "tcp"
      ports = [allow.value]
    }
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["web"]
}

# allow ssh traffic
resource "google_compute_firewall" "allow-ssh" {
  name = "${var.app_name}-fw-allow-ssh"
  network = google_compute_network.vpc.name
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = ["109.252.65.0/24"]
  target_tags = ["ssh"]
}