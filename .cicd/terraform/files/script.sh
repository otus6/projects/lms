#!/bin/bash

apt update
apt install -y docker docker-compose
docker login -u ${user} -p ${token} ${registry_url}
docker-compose -p ${project_name} -f ingress-compose-up.yaml up -d --force-recreate
docker-compose -p ${project_name} -f flask-compose-up.yaml up -d --force-recreate
