# DNS binding
resource "google_dns_record_set" "lms-terraform-ansible" {
    name = "${var.app_name}.${google_dns_managed_zone.inpublic.dns_name}"
    type = "A"
    ttl  = 300

    managed_zone = google_dns_managed_zone.inpublic.name
    rrdatas = [google_compute_instance.lms-terraform-ansible.network_interface[0].access_config[0].nat_ip]

    lifecycle {
        ignore_changes = []
        create_before_destroy = true
    }
}

# Zone creating
resource "google_dns_managed_zone" "inpublic" {
    name     = "inpublic"
    dns_name = "inpublic.net."
}

