resource "google_compute_instance" "lms-terraform-ansible" {
  name         = var.app_name
  zone         = "${var.gcp_region}-a"
  machine_type = "e2-micro"

  tags = ["web", "ssh"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

    network_interface {
    network     = google_compute_network.vpc.name
    subnetwork  = google_compute_subnetwork.public_subnet.name
    access_config {
      # nat_ip = google_compute_address.static.address
    }
  }

  metadata = {
    ssh-keys = "deploy:${file(var.public_key_path)}"
  }
}

resource "local_file" "hosts" {
  # filename    = "../ansible/inventories/dev/hosts"
  filename    = var.hosts_path
  content     = templatefile("files/hosts.tpl", {
      vm_ip   = google_compute_instance.lms-terraform-ansible.network_interface.0.access_config.0.nat_ip
      vm_name = google_compute_instance.lms-terraform-ansible.name
      vm_user = var.deploy_user
    })
}

resource "local_file" "host_script" {
    filename        = "../ansible/inventories/add_host.sh"
    file_permission = "0700"
    content         = <<-EOF
    #!/bin/sh

    echo "Setting SSH Key"
    ssh-add ~/.ssh/id_rsa.pub
    echo "Adding entry for instance IP to known_hosts file"
    ssh-keyscan -H ${google_compute_instance.lms-terraform-ansible.network_interface.0.access_config.0.nat_ip} >> ~/.ssh/known_hosts

    EOF

}