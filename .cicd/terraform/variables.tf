variable "app_name" {
  type = string
  # default = "lms-terraform-ansible"
}

variable "gcp_region" {
  type = string
  default = "europe-west6"
}

variable "public_subnet_cidr" {
  type = string
  default = "10.10.1.0/24"
}

variable "gcp_project" {
  type = string
}

variable "public_key_path" {
  description = "Path to file containing public key"
}

variable "private_key_path" {
  description = "Path to file containing private key"
}

variable "hosts_path" {
  description = "Path to file inventory file"
}

# CI registry credentials
variable "token" {
  description = "Gitlab registry deploy token"
}

variable "user" {
  description = "Gitlab deploy user"
}

variable "registry_url" {
  description = "Gitlab registry URL"
}

variable "image_name" {
  description = "Image name of flask app"
}

variable "project_name" {
  description = "Compose project name"
}

variable "virtual_host" {
  description = "DNS hostname"
}

variable "deploy_user" {
  description = "Gitlab deploy user"
}